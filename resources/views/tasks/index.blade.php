@extends('layouts.main')

@section('title', 'Tasks Home')

@section('content')

    <div class="row justify-content-center mb-3">
        <div class="col-sm-4">
            <a href="{{route('task.create')}}" class="btn btn-block btn-success">Create Task</a>
        </div>

    </div>

    @if($tasks->count() == 0)
        <p class="lead text-center"> There are no tasks listed. Why don't create one!</p>
    @else
        <div class="grid-container">
            @foreach($tasks as $task)
                    <div class="card" style="max-width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title"> {{ $task->name }} </h5>
                            <h6 class="card-subtitle mb-2 text-muted"> {{ $task->due_date }}</h6>
                            <p class="card-text"> {{ $task->description }}</p>
                            <p class="card-text"><small class="text-muted">Last {{ $task->created_at }}</small></p>

                            {!! Form::open(['route' => ['task.destroy', $task->id],'method' => 'DELETE']) !!}
                                <a href="{{ route('task.edit', $task->id) }}" class="btn btn-sm btn-primary">
                                    <span class="glyphicon glyphicon-edit"></span>Edit</a>
                                <button type="submit" class="btn-sm btn-danger">Delete</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
            @endforeach
        </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-sm-6 text-center">
            {{ $tasks->links() }}
        </div>
    </div>
@endsection
