# ToDo List

###### Soltanagha Huseynov

## Summary
Simply create any task. View, edit and manage tasks on the go from the web site.

## Goals

ToDo List functions:

* Add details about the work you need to focus on
* Edit Details about any task as your work progresses
* Set a due date for every task to help you achieve your goals

## Quick Installation

    1. git clone https://gitlab.com/soltanagha.huseynov/todo-list.git;

    2. cd todo-list

    3. composer install

        3.1. Amend .env file settings to your database

    4. php artisan migrate

    5. php artisan serve